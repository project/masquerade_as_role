<?php

namespace Drupal\Tests\masquerade_as_role\Functional;

use Drupal\Tests\Traits\Core\CronRunTrait;
use Drupal\Tests\masquerade\Functional\MasqueradeWebTestBase;

/**
 * Tests form permissions and user switching functionality.
 *
 * @group masquerade_as_role
 */
class MasqueradeAsRoleTest extends MasqueradeWebTestBase {
  use CronRunTrait;
  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'masquerade',
    'masquerade_as_role',
    'user',
    'block',
  ];

  /**
   * Tests masquerade user links.
   */
  public function testMasqueradeAsRole():void {
    $this->drupalLogin($this->adminUser);

    // Verify that a token is required.
    $this->drupalGet('user/0/masquerade');
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet('user/' . $this->authUser->id() . '/masquerade');
    $this->assertSession()->statusCodeEquals(403);

    // Verify that the admin user is able to masquerade as editor,
    // moderator roles.
    $this->assertSessionByUid($this->adminUser->id());
    $tmp_user = $this->createUserWithRole([
      $this->editorUser,
      $this->moderatorUser,
    ]);
    $this->masqueradeAs($tmp_user);
    $this->assertSessionByUid($tmp_user->id(), $this->adminUser->id());
    $this->assertNoSessionByUid($this->adminUser->id());

    // Verify that a token is required to unmasquerade.
    $this->drupalGet('unmasquerade');
    $this->assertSession()->statusCodeEquals(403);

    // Verify that the web user cannot masquerade.
    $this->drupalGet('user/' . $this->adminUser->id() . '/masquerade', [
      'query' => [
        'token' => $this->drupalGetToken('user/' . $this->adminUser->id() . '/masquerade_as_role'),
      ],
    ]);
    $this->assertSession()->statusCodeEquals(403);

    // Verify that the user can unmasquerade.
    $this->unmasquerade($tmp_user);
    $this->assertNoSessionByUid($tmp_user->id());
    $this->assertSessionByUid($this->adminUser->id());
  }

  /**
   * Clean up test (when switching back).
   *
   * A temporary user is created when masquerading as role.
   * And deleted when switching back.
   */
  public function testCleanupRegular():void {
    $this->drupalLogin($this->adminUser);
    $original = $this->masqueradeCountUser();
    // Masquerade as role + assert number of users has been incremented.
    $account = $this->createUserWithRole([
      $this->editorUser,
      $this->moderatorUser,
    ]);
    $this->masqueradeAs($account);
    $this->assertEquals($this->masqueradeCountUser(), $original + 1);
    // Switch back + assert number of users is the same as initially.
    $this->unmasquerade($account);
    $this->assertEquals($this->masqueradeCountUser(), $original);

  }

  /**
   * Clean up test (when no switching back).
   *
   * Cron takes care of deleting temporary accounts.
   * When the user did not switch back.
   */
  public function testMasqueradeAsRoleCron():void {
    $this->drupalLogin($this->adminUser);
    $original = $this->masqueradeCountUser();
    // Masquerade as role + assert number of users has been incremented.
    $account = $this->createUserWithRole([
      $this->editorUser,
      $this->moderatorUser,
    ]);
    $this->masqueradeAs($account);
    $this->assertEquals($this->masqueradeCountUser(), $original + 1);
    // Log out + assert number of users has not been decremented.
    $this->drupalLogout();
    $this->assertEquals($this->masqueradeCountUser(), $original + 1);
    $this->cronRun();
    // Run cron + assert number of users is the same as initially.
    $this->assertEquals($this->masqueradeCountUser(), $original);
  }

  /**
   * Helper - Get number of users.
   */
  protected function masqueradeCountUser():int {
    $query = \Drupal::entityQuery('user');
    return $query->accessCheck(TRUE)->count()->execute();
  }

  /**
   * Create user by given role.
   *
   * @param mixed[] $roles
   *   The roles the user will be assigned to masquerade as.
   *
   * @return \Drupal\user\Entity\User|false
   *   Newly created user object.
   */
  protected function createUserWithRole(array $roles) {
    $user = $this->drupalCreateUser();
    $tmp_username = substr(md5(microtime()), rand(0, 26), 8);
    $user->setUsername($tmp_username);
    $user->setEmail($tmp_username . 'masquerade_as_role@example.com');
    foreach ($roles as $role) {
      $user->addRole($role->id());
    }
    $user->save();
    return $user;
  }

}
