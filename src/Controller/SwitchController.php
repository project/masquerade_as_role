<?php

namespace Drupal\masquerade_as_role\Controller;

use Drupal\masquerade\Controller\SwitchController as MasqueradeSwitchController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller for switch and back to masquerade as user.
 */
class SwitchController extends MasqueradeSwitchController {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container):self {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * Allows a user who is currently masquerading to become a new user.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirect response to previous page.
   *
   * @see this::getRedirectResponse()
   */
  public function switchBack(Request $request) {
    // Store current user name for messages.
    $account_name = $this->currentUser->getDisplayName();
    $masqueraded_user = $this->currentUser->id();
    $masqueraded_mail = $this->currentUser->getEmail();

    if ($this->masquerade->switchBack()) {
      $this->messenger()->addStatus($this->t('You are no longer masquerading as @user.', [
        '@user' => $account_name,
      ]));
    }
    else {
      $this->messenger()->addError($this->t('Error trying unmasquerading as @user.', [
        '@user' => $account_name,
      ]));
    }
    if (strpos($masqueraded_mail, 'masquerade_as_role') !== FALSE) {
      $this->entityTypeManager->getStorage('user')->load($masqueraded_user)->delete();
    }
    return $this->getRedirectResponse($request);
  }

}
