<?php

namespace Drupal\masquerade_as_role;

use Drupal\Core\Block\BlockBase;
use Drupal\masquerade_as_role\Form\MasqueradeAsRoleForm;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Helper class to override any block content in a HOOK_block_view_alter().
 */
class MasqueradeAsRoleOverrideBlock extends BlockBase {

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * Constructor.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Container.
   */
  public function __construct(ContainerInterface $container) {
    $this->formBuilder = $container->get('form_builder');
  }

  /**
   * {@inheritdoc}
   */
  public function getPlugin() :self {
    return $this;
  }

  /**
   * {@inheritdoc}
   *
   * @return mixed[]
   *   The form array.
   */
  public function build(): array {
    return $this->formBuilder->getForm(MasqueradeAsRoleForm::class);
  }

}
