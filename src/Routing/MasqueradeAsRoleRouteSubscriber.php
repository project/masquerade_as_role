<?php

namespace Drupal\masquerade_as_role\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class MasqueradeAsRoleRouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) :void {
    // Change the page callback for unmasquerade.
    $route = $collection->get('masquerade.unmasquerade');
    $route->setDefault('_controller', '\\Drupal\\masquerade_as_role\\Controller\\SwitchController::switchBack');
  }

}
