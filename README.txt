
-- SUMMARY --

This module allows a user to masquerade as a set of roles
(instead of masquerading as an existing user).
Under the hood, a temporary account with the appropriate roles
 is created on the fly and deleted when switching back.

If users do not switch back, temporary accounts are deleted by cron.

For a full description of the module, visit the project page:
  https://www.drupal.org/project/masquerade_as_role

To submit bug reports and feature suggestions, or to track changes:
  https://www.drupal.org/project/issues/masquerade_as_role

-- REQUIREMENTS --

Masquerade, https://www.drupal.org/project/masquerade.


-- INSTALLATION --

* Install as usual, see
  http://drupal.org/documentation/install/modules-themes/modules-8
