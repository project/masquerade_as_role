(function ($) {

  /**
   * Toggle masquerade_user_field / masquerade_role_field.
   */
  Drupal.behaviors.masqueradeAsRoleToggle = {
    attach: function (context, settings) {

      /*
       * Empty masquerade_role_field when editing masquerade_user_field.
       */
      $('#edit-masquerade-as').click(function() {
        $('#edit-masquerade-as-role').val([]);
      });

      /*
       * Empty masquerade_user_field when editing masquerade_role_field.
       */
      $('#edit-masquerade-as-role').click(function() {
        $('#edit-masquerade-as').val('');
      });

    }
  };

})(jQuery);
