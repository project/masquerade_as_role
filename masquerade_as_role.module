<?php

/**
 * @file
 * Allows privileged users to masquerade as another user given specific role.
 */

use Drupal\Core\Block\BlockPluginInterface;
use Drupal\masquerade_as_role\MasqueradeAsRoleOverrideBlock;

/**
 * Implements hook_cron().
 *
 * Clean up temporary users with invalid sessions.
 *
 * I.e. when people didn't use the switch back link.
 */
function masquerade_as_role_cron():void {
  // Watch http://drupal.org/node/268487 before modifying this query.
  $database = \Drupal::database();
  $subquery = $database->select('sessions', 's');
  $subquery->addField('s', 'uid');
  $query = \Drupal::entityQuery('user')
    ->condition('mail', '%' . $database->escapeLike("masquerade_as_role@example.com") . '%', 'LIKE')
    ->condition('uid', $subquery, 'NOT IN')
    ->accessCheck(FALSE);
  $uids = $query->execute();
  $itemsToDelete = \Drupal::entityTypeManager()->getStorage('user')
    ->loadMultiple($uids);
  // Loop through our entities and deleting them by calling by delete method.
  foreach ($itemsToDelete as $item) {
    $item->delete();
  }
}

/**
 * Implements hook_block_view_alter().
 */
function masquerade_as_role_block_view_alter(array &$build, BlockPluginInterface $block):void {
  if ($block->getBaseId() === 'masquerade') {
    $build['#block'] = new MasqueradeAsRoleOverrideBlock(\Drupal::getContainer());
  }
}
